from flask import request, jsonify, url_for
from app.models import Car
from app.api import bp
from app import db
from app.api.errors import bad_request

@bp.route('/cars/<int:id>', methods=['GET'])
def get_car(id):
    # Возвращаем пользователю информацию о машине
    # Если машины с таким уникальным идентификатором нет,
    # возвращаем HTTP 404
    return jsonify(Car.query.get_or_404(id).to_dict())

@bp.route('/cars', methods=['GET'])
def get_cars():
    # Какую страницу показать? По умолчанию 1
    page = request.args.get('page', 1, type=int)
    # Сколько элементов на странице? По умолчанию 10
    # Но не больше 100
    per_page = min(request.args.get('per_page', 10), 100)
    # Генерируем набор данных для страницы
    data = Car.to_collection_dict(Car.query, page, per_page, 'api.get_cars')
    # Возвращаем пользователю json
    return jsonify(data)

@bp.route('/cars', methods=['POST'])
def create_car():
    # Проверяем, что запрос пришел с телом
    data = request.get_json() or {}
    # Если тела запроса нет, возвращаем ошибку
    if not data:
        return bad_request('Car should contain something')
    # Создаем экземпляр модели ORM для машины
    car = Car()
    # Загружаем данные из тела запроса в экземпляр
    car.from_dict(data)
    # Добавляем машину к текущей сессии с БД
    db.session.add(car)
    # Комиттим транзакцию в БД
    db.session.commit()
    # По стандарту мы должны вернуть объект
    # с присвоенным уникальным идентификатором
    response = jsonify(car.to_dict())
    # Так же по стандарту код ответа должен быть 201 вместо 200
    # 200 OK
    # 201 Created
    response.status_code = 201
    # В заголовке передаем ссылку на созданный объект
    response.headers['Location'] = url_for('api.get_car', id=car.id)
    return response

@bp.route('/cars/<int:id>', methods=['PUT'])
def update_car(id):
    car = Car.query.get_or_404(id)
    data = request.get_json() or {}
    if not data:
        return bad_request('Build should contain something')
    car.from_dict(data)
    db.session.commit()
    return jsonify(car.to_dict())
    
@bp.route('/cars/<int:id>', methods=['DELETE'])
def remove_car(id):
    car = Car.query.get_or_404(id)
    db.session.delete(car)
    db.session.commit()
    return jsonify(car.to_dict())